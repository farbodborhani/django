from django.db import models

class TYP(models.Model):
    namecourse=models.CharField(max_length=50)
    def __str__(self):
        return self.namecourse

class COURSE(models.Model):
    code_name=models.IntegerField()
    nam=models.CharField(max_length=50)
    disc=models.TextField(default="")
    pic=models.ImageField(upload_to="course/images",default="")
    season=models.FileField(default="")
    cost=models.FloatField()
    namecourse=models.ForeignKey(TYP,on_delete=models.CASCADE,default="")


class Author(models.Model):
    first_name=models.CharField(max_length=30)
    last_name=models.CharField(max_length=40)
    email=models.EmailField()

class Publisher(models.Model):
    name=models.CharField(max_length=30)
    address=models.CharField(max_length=50)
    city=models.CharField(max_length=60)
    state_province=models.CharField(max_length=30)
    country=models.CharField(max_length=50)
    website=models.URLField()


class Book(models.Model):
    title=models.CharField(max_length=100)
    authors=models.ManyToManyField(Author)
    publisher=models.ForeignKey(Publisher,on_delete=models.CASCADE)
    time_date=models.DateField()        
