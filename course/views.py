from django.shortcuts import render,redirect
from .forms import CREATECOURSE
from . import models

def add_user(request):
    form=CREATECOURSE()
    if(request.method=='POST'):
        form=CREATECOURSE(request.POST,request.FILES)
        if(form.is_valid()):
            print('yes its done')
            form.save()
            return redirect('course:data')

    return render(request,'course/add.html',{'form':form})


def done_page(request):
    return render(request,'course/after.html')

def data_page(request):
    delete_data=request.GET.get('delete-Text')
    get_data=models.COURSE.objects.filter(nam=delete_data).delete()
    data=models.COURSE.objects.all()
    return render(request,'course/data_page.html',{'data':data})


def search_page(request):
    data=request.GET.get('Text-search')
    search=models.COURSE.objects.filter(nam=data)
    x=list(search)
    if(len(x)==0):
        text='None Data'
        return render(request,'course/search_page.html',{'text':text})
    return render(request,'course/search_page.html',{'search':search})

def create_page(request):

    return render(request,'course/create_page.html')

def edit_page(request):
    data_update=request.GET.get('edit-data')
    data_update_text=request.GET.get('Update-Text')
    form=models.COURSE.objects.filter(nam=data_update).update(nam=data_update_text)
    data=models.COURSE.objects.all()
    return render(request,'course/data_page.html',{'data':data})
