from django.contrib import admin
from .models import TYP,COURSE,Author,Book,Publisher
# Register your models here.


admin.site.register(TYP)
admin.site.register(COURSE)
admin.site.register(Author)
admin.site.register(Book)
admin.site.register(Publisher)
