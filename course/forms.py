from django import forms
from . import models


class CREATECOURSE(forms.ModelForm):

    class Meta:
        model=models.COURSE
        fields=['code_name','nam','disc','pic','season','cost','namecourse']
