from django.contrib import admin
from django.urls import path
from . import views
app_name='course'
urlpatterns = [
    path('add/',views.add_user,name='add'),
    path('done/',views.done_page,name='done'),
    path('data/',views.data_page,name='data'),
    path('search/',views.search_page,name="search"),
    path('create/',views.create_page,name="create"),
    path('edit/',views.edit_page,name="edit"),

]
